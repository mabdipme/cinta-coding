import React from 'react'

const Login = () => {
  return (
    <>
      <section className='p-5 flex flex-col items-center justify-center'>
        <div className='mt-28'>
          <h1 className='font-bold text-xl'>Login Page</h1>
        </div>
        <div className="w-full max-w-xs mt-24">
          <form className="rounded px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <input className="shadow appearance-none border border-blue-300 rounded-2xl w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username" />
            </div>
            <div className="mb-6">
              <input className="shadow appearance-none border border-blue-300 rounded-2xl  w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder='Password' />
            </div>
            <div className="flex items-center w-full justify-between">
              <button className="bg-blue-500 w-full rounded-2xl hover:bg-blue-700 text-white font-bold py-2 px-4 focus:outline-none focus:shadow-outline" type="button">
                Login
              </button>
            </div>
          </form>
        </div>
      </section>
    </>
  )
}

export default Login