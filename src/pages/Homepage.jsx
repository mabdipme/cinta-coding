import React, { useEffect } from 'react'
import { HomePic } from '../assets'
import Layout from '../components/Layouts/Layout'

const Homepage = () => {
  useEffect(() => {
    return () => {
      document.title = 'Cinta Coding'
    };
  }, [])
  return (
    <Layout>
      <section className='p-5'>
        <div className='flex justify-center'>
          <img src={HomePic} alt='homepage-pic' width={900} />
        </div>
      </section>
    </Layout>
  )
}

export default Homepage