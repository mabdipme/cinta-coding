import React from 'react'
import Header from '../Header'

const Layout = (props) => {
  const { children } = props
  return (
    <>
      <Header title='Cinta Coding' />
      {children}
    </>
  )
}

export default Layout