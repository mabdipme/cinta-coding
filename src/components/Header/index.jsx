import React from 'react'
import { Link } from 'react-router-dom'

const Header = ({ title }) => {
  return (
    <>
      <header className='p-3 bg-white flex flex-row justify-between'>
        <title>{title}</title>
        <div className='flex'>
          <span className='my-2 mx-2 text-lg font-bold'>Cinta Coding</span>
        </div>
        <div className='flex flex-shrink'>
          <div className='p-2 mx-2'>
          </div>
          <div className='p-2 bg-blue-600 rounded-lg w-24 text-white font-semibold text-center mx-2 h-full'>
            <Link type='button' to='/login'>
              Login
            </Link>
          </div>
        </div>
      </header>
    </>
  )
}

export default Header